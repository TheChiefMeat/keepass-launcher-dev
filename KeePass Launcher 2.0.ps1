# (C) TheChiefMeat 2016. All rights reserved.
#
# DISCLAIMER/LICENCE
#
# WHILE I HAVE MADE EVERY POSSIBLE EFFORT TO ENSURE THIS SOFTWARE IS BUG FREE,
# I DO NOT GUARANTEE THAT IT IS FREE FROM DEFECTS. THIS SCRIPT IS PROVIDED "AS IS"
# AND YOU USE THIS SCRIPT AT YOUR OWN RISK. UNDER NO CIRCUMSTANCE SHALL I BE HELD
# LIABLE FOR ANY DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES, LOSSES OR THEFT FROM USE
# OR MISUSE OF THIS SOFTWARE.
#
# FOR PRIVATE USE YOU MAY:
# MODIFY THIS SOFTWARE ANY WAY YOU SEE FIT, HOWEVER SAID MODIFIED VERSIONS ARE NOT TO
# BE REDISTRIBUTED VIA ANY MEANS.
#
# FOR PRIVATE USE YOU MAY NOT:
# REDISTRIBUTE THIS SOFTWARE.
# REDISTRIBUTE MODIFIED VERSIONS OF THIS SOFTWARE.
# COMMERCIALISE THIS SOFTWARE.
# SELL THIS SOFTWARE.
#
# ANY COMMERCIAL USE OF THIS SOFTWARE IS STRICTLY PROHIBITED.
# ANY COMMERCIALISATION OF THIS SOFTWARE IS STRICTLY PROHIBITED.
#
# ALL DOWNLOAD LINKS MUST POINT DIRECTLY TO THE OFFICIAL BITBUCKET REPOSITORY.
#
# OFFICIAL BITBUCKET REPOSITORY:
#
# https://bitbucket.org/TheChiefMeat/keepass-launcher/downloads/keepass-launcher-latest.zip
#
# -----------------------------------------------------------------------TITLE WINDOW NAME--------------------------------------------------------

$Host.UI.RawUI.WindowTitle = 'KeePass Launcher 2.0'

#-----------------------------------------------------------------------ERROR NULLIFICATION-------------------------------------------------------

$ErrorActionPreference= 'silentlycontinue'

#-------------------------------------------------------------------------UI CONFIGURATION--------------------------------------------------------

#Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser

$console = $host.UI.RawUI
Import-Module .\Modules\SetConsoleFont.psm1

#-----------------------------------------------------------------------CONFIGURATION FILE---------------------------------------------------------

$Config = "KeePassLauncher.Config"
If (Test-Path $Config){
}Else{
  write-output{[KeePassLauncherSettings]
ApplicationInstallLocation=
DatabaseLocation=
ForcedLogin=NO
PauseTime=9999

[KeyfileSettings]
KEYFILE=YES
KEYFILEONLY=NO
KEYFILELocation=

[EncryptionFileSettings]
KeePassUsrDataLocation=KeePassUsrData.xml
KeePassUsrDataLocation2=2
KeePassUsrDataLocation3=3
KeePassUsrDataLocation4=4
KeePassUsrDataLocation5=5

[BackupSettings]
Backup=NO
BackupFolder=Z:\KeePass\Backup

[ConsoleSettings]
Background=Black
Foreground=White
FontSize=-1

[Startup]
Startup=0
VERSION=} | add-content "KeePassLauncher.Config"
}

$file = get-content "KeePassLauncher.Config"
$file | foreach {
  $items = $_.split("=")
  if ($items[0] -eq "ApplicationInstallLocation"){$ApplicationInstallLocation = $items[1]}
  if ($items[0] -eq "DatabaseLocation"){$KeePassDataBaseLocation = $items[1]}
  if ($items[0] -eq "ForcedLogin"){$ForcedLogin = $items[1]}
  if ($items[0] -eq "PauseTime"){$PauseTime = $items[1]}
  if ($items[0] -eq "KEYFILE"){$KEYFILE = $items[1]}
  if ($items[0] -eq "KEYFILEONLY"){$KEYFILEONLY = $items[1]}
  if ($items[0] -eq "KEYFILELocation"){$KEYFILELocation = $items[1]}
  if ($items[0] -eq "KeePassUsrDataLocation"){$KeePassUsrDataLocation = $items[1]}
  if ($items[0] -eq "KeePassUsrDataLocation2"){$KeePassUsrDataLocation2 = $items[1]}
  if ($items[0] -eq "KeePassUsrDataLocation3"){$KeePassUsrDataLocation3 = $items[1]}
  if ($items[0] -eq "KeePassUsrDataLocation4"){$KeePassUsrDataLocation4 = $items[1]}
  if ($items[0] -eq "KeePassUsrDataLocation5"){$KeePassUsrDataLocation5 = $items[1]}
  if ($items[0] -eq "Backup"){$Backup = $items[1]}
  if ($items[0] -eq "BackupFolder"){$BackupFolder = $items[1]}
  if ($items[0] -eq "Background"){$Background = $items[1]}
  if ($items[0] -eq "Foreground"){$Foreground = $items[1]}
  if ($items[0] -eq "FontSize"){$FontSize = $items[1]}
  if ($items[0] -eq "Startup"){$Startup = $items[1]}
  if ($items[0] -eq "VERSION"){$VERSION = $items[1]}
}

#-----------------------------------------------------------------------------------------CONSOLE CONFIG------------------------------------------------------------------------------

$console.ForegroundColor = $Foreground
$console.BackgroundColor = $Background

Set-ConsoleFont $FontSize
cls

#-------------------------------------------------------------------------------------------DATABASE NAMING----------------------------------------------------------------------------------

$DatabaseName = Split-Path $KeePassDataBaseLocation -leaf

#--------------------------------------------------------------------------------------STARTUP & SHORTCUT CREATION---------------------------------------------------------------------------

If ($Startup -eq "0"){
  (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "Startup=0", "Startup=1"} | set-content "KeePassLauncher.Config"
  (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "PauseTime=9999", "PauseTime=1000"} | set-content "KeePassLauncher.Config"

  & regedit.exe /s "Extras\FixRunPowerShellScriptWithSpacesInPathProblem.reg"
  & regedit.exe /s "Extras\ElevatePermissions.reg"


  $directorypath = (Get-Item -Path ".\" -Verbose).FullName

  $WshShell = New-Object -ComObject WScript.Shell
  $Shortcut = $WshShell.CreateShortcut("$Home\Desktop\KeePass Launcher.lnk")
  $Shortcut.TargetPath = $directorypath + '\KeePass Launcher 2.0.exe'
  $Shortcut.IconLocation =  $directorypath + "\KeePass Launcher 2.0.exe,0"
  $Shortcut.Description ="KeePass Launcher"
  $Shortcut.WorkingDirectory = $directorypath
  $Shortcut.Save()

  #----------------------------------------------------------------------------------------------FILE SELECTION----------------------------------------------------------------------------------

  $size = $console.WindowSize
  $size.Width = 87
  $size.Height = 8
  $console.WindowSize = $size

  Write-Output "


  FIRST TIME USE: Please select your KeePass Manager, KeePass Database and Keyfile..."
  sleep 2

[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
$SelectKeePassexe = New-Object System.Windows.Forms.OpenFileDialog

$SelectKeePassexe.Title = "Please select your KeePass executable (KeePass.exe/KeeWeb.exe/KeePassX.exe)"
$SelectKeePassexe.InitialDirectory = "C:\"
$SelectKeePassexe.Filter = "KeePass executable (*.exe)|*exe"
$result = $SelectKeePassexe.ShowDialog()
$inputFile = $SelectKeePassexe.FileName

  (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "ApplicationInstallLocation=", "ApplicationInstallLocation=$inputFile"} | set-content "KeePassLauncher.Config"

  $InstallationType = Split-Path $inputFile -leaf

  If ($InstallationType -eq "KeePass.exe"){
      (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "VERSION=", "VERSION=$InstallationType"} | set-content "KeePassLauncher.Config"
  }
  If ($InstallationType -eq "KeeWeb.exe"){
      (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "VERSION=", "VERSION=$InstallationType"} | set-content "KeePassLauncher.Config"
  }
  If ($InstallationType -eq "KeePassX.exe"){
      (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "VERSION=", "VERSION=$InstallationType"} | set-content "KeePassLauncher.Config"
  }

[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
$SelectKeePassDatabase = New-Object System.Windows.Forms.OpenFileDialog

$SelectKeePassDatabase.Title = "Please select your KeePass Database"
$SelectKeePassDatabase.InitialDirectory = "C:\"
$SelectKeePassDatabase.Filter = "KeePass Database (*.kdbx)|*kdbx"
$result = $SelectKeePassDatabase.ShowDialog()
$inputFile = $SelectKeePassDatabase.FileName

  (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "DatabaseLocation=", "DatabaseLocation=$inputFile"} | set-content "KeePassLauncher.Config"

[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
$SelectKeePassKeyfile = New-Object System.Windows.Forms.OpenFileDialog

$SelectKeePassKeyfile.Title = "Please select your Keyfile"
$SelectKeePassKeyfile.InitialDirectory = "C:\"
$SelectKeePassKeyfile.Filter = "KeePass Keyfile (*.key)|*key"
$result = $SelectKeePassKeyfile.ShowDialog()
$inputFile = $SelectKeePassKeyfile.FileName

  (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "KEYFILELocation=", "KEYFILELocation=$inputFile"} | set-content "KeePassLauncher.Config"

  If ($inputFile -eq ""){
    (get-content "KeePassLauncher.Config") | foreach-object {$_ -replace "KEYFILE=YES", "KEYFILE=NO"} | set-content "KeePassLauncher.Config"
  }
cls

  Write-Output "


  FIRST TIME USE: Please ensure KeePassLauncher.Config file is configured correctly..."
  sleep 4
  Exit
}

#---------------------------------------------------------------------PASSWORD ENCRYPTION-----------------------------------------------------------

If (Test-Path $KeePassUsrDataLocation){
}Else{

  $size = $console.WindowSize
  $size.Width = 80
  $size.Height = 9
  $console.WindowSize = $size

Write-Output "


      Please Enter Database MASTER Password (Username does not matter)...

 Note: Deleting the KeePassUsrData.xml file deletes the stored MASTER PASSWORD!"

sleep 5
cls
$Password = $host.ui.PromptForCredential("", "        Please enter your desired username and database MASTER PASSWORD.", "", "")
$Password | Export-Clixml -Path "$KeePassUsrDataLocation"
cls

Write-Output "



                Setup Complete! Please restart KeePass Launcher..."
sleep 3
Exit
}

#-----------------------------------------------------------------------------MAIN UI SIZE----------------------------------------------------------

$size = $console.WindowSize
$size.Width = 40
$size.Height = 8
$console.WindowSize = $size

#-------------------------------------------------------------KEEPASS DATABASE & INSTALLATION DETECTION---------------------------------------------

If (Test-Path $ApplicationInstallLocation){
      }Else{
         Write-Output "


           Program Not Found!..."
           Sleep 3
           Exit
          }

If ($ApplicationInstallLocation -eq ""){
   Write-Output "


           Program Not Found!..."
      Sleep 3
      Exit
    }

If (Test-Path $KeePassDataBaseLocation){
     }Else{
      Write-Output "


          Database Not Found!..."
      Sleep 3
      Exit
     }

If ($KeePassDataBaseLocation -eq ""){
    Write-Output "


          Database Not Found!..."
      Sleep 3
      Exit
    }

#----------------------------------------------------------------------------DATABASE BACKUP-----------------------------------------------------------------

If ($Backup -eq "NO"){
}else{

    $date=(get-date -Format d) -replace("/")
    $time=(get-date -Format t) -replace(":")
    $source = $KeePassDataBaseLocation
    $newfilename = "$date"+"_"+"$time"+".kdbx"

    If (Test-Path $BackupFolder){
      }else{
        New-Item -ItemType Directory -Force -Path $BackupFolder | Out-Null
    }
    If (Test-Path "$BackupFolder\$newfilename"){
    }else{
      Copy-Item $KeePassDataBaseLocation $BackupFolder
      If (Test-Path "$BackupFolder\$DatabaseName"){
        Rename-Item "$BackupFolder\$DatabaseName" -NewName $newfilename
      }
    }
  }

#-------------------------------------------------------------KEEPASS & KEEWEB PROCESS DETECTION------------------------------------------------------

If ($ForcedLogin -eq "YES"){
}Else{
$KeePassRunning = Get-Process Keepass -ErrorAction SilentlyContinue
if ($KeePassRunning) {

Write-Output "


      KeePass is already running..."
  Sleep 2
  Exit
}
}


If ($ForcedLogin -eq "YES"){
}Else{
$KeeWebRunning = Get-Process KeeWeb -ErrorAction SilentlyContinue
if ($KeeWebRunning) {

Write-Output "


      KeeWeb is already running..."
  Sleep 2
  Exit
}
}

If ($ForcedLogin -eq "YES"){
}Else{
$KeePassXRunning = Get-Process KeePassX -ErrorAction SilentlyContinue
if ($KeePassXRunning) {

Write-Output "


      KeePassX is already running..."
  Sleep 2
  Exit
}
}
#--------------------------------------------------------------------KEEPASS LOGIN AUTOMATION ---------------------------------------------------------
Function KeePassAutomation{

If ($VERSION -eq "KeePass.exe"){

If ($KEYFILEONLY -eq "YES"){
  If ($KEYFILE -eq "NO"){

  Write-Output "


      Please enable Keyfile mode..."
       Sleep 3
       Exit
  }Else{

  If (Test-Path $KEYFILELocation) {

  Write-Output "


       Logging you into KeePass..."

  & $KeePassDataBaseLocation
  Start-Sleep -m $PauseTime
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
  1..4 | % {
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  Start-Sleep -m $PauseTime
  [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  1..2 | % {
  [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  EXIT
}
}
}

If ($KEYFILE -eq "NO"){

  Write-Output "


       Logging you into KeePass..."

       & $KeePassDataBaseLocation
       Start-Sleep -m $PauseTime
       [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
       [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
       [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
       $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
       [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
       [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
       1..3 | % {
       [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
       [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
       [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
       [System.Windows.Forms.SendKeys]::SendWait("{UP}")
       Start-Sleep -m $PauseTime
       1..3 | % {
       [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
       [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
       [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
       Exit
     }Else{
}

If ($KEYFILE -eq "YES"){
  If (Test-Path $KEYFILELocation) {

  Write-Output "


       Logging you into KeePass..."

  & $KeePassDataBaseLocation
  Start-Sleep -m $PauseTime
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
  $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
  1..4 | % {
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  Start-Sleep -m $PauseTime
  [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  Start-Sleep -m $PauseTime
  1..2 | % {
  [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
  [Microsoft.VisualBasic.Interaction]::AppActivate("Open Database - $DatabaseName")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  Exit
  }Else{

    Write-Output "


      Please Insert Security USB..."
      Sleep 3
      Exit
  }
}
#----------------------------------------------------------------------------KEEWEB LOGIN AUTOMATION-----------------------------------------------------
}ElseIf ($VERSION -eq "KeeWeb.exe"){
  If ($ForcedLogin -eq "YES"){

    Write-Output "


    ForcedLogin mode not supported..."
      Sleep 3
      Exit
    }

  If ($KEYFILEONLY -eq "YES"){

    If ($KEYFILE -eq "NO"){

    Write-Output "


      Please enable Keyfile mode..."
         Sleep 3
         Exit
    }Else{

    If (Test-Path $KEYFILELocation) {

      Write-Output "


       Logging you into KeeWeb..."

    & $KeePassDataBaseLocation
    Start-Sleep -m $PauseTime
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
    1..2 | % {
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")}
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -m $PauseTime
    [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -m $PauseTime
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    EXIT
  }
 }
}

  If ($KEYFILE -eq "NO"){

    Write-Output "


       Logging you into KeeWeb..."

  & $KeePassDataBaseLocation
  Start-Sleep -m $PauseTime
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
  [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
  [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
  $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
  [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
  [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
  Start-Sleep -m $PauseTime
  [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
  [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
  Exit
  }Else{
  }

  If ($KEYFILE -eq "YES"){
    If (Test-Path $KEYFILELocation) {

      Write-Output "


       Logging you into KeeWeb..."

    & $KeePassDataBaseLocation
    Start-Sleep -m $PauseTime
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
    $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
    1..2 | % {
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -m $PauseTime
    [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Start-Sleep -m $PauseTime
    [Microsoft.VisualBasic.Interaction]::AppActivate("KeeWeb")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    EXIT
    }Else{

      Write-Output "


      Please Insert Security USB..."
        Sleep 3
        Exit
    }
  }
#----------------------------------------------------------------KEEPASSX LOGIN AUTOMATION--------------------------------------------------
  }Else{
    If ($VERSION -eq "KeePassX.exe"){
      If ($ForcedLogin -eq "YES"){

        Write-Output "


    ForcedLogin mode not supported..."
          Sleep 3
          Exit
        }

    If ($KEYFILEONLY -eq "YES"){

      If ($KEYFILE -eq "NO"){

      Write-Output "


      Please enable Keyfile mode..."
           Sleep 3
           Exit
      }Else{

      If (Test-Path $KEYFILELocation) {

        Write-Output "


       Logging you into KeePassX..."

      & $KeePassDataBaseLocation
      Start-Sleep -m $PauseTime
      [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
      1..4 | % {
      [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")}
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      Start-Sleep -m $PauseTime
      [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      Start-Sleep -m $PauseTime
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      EXIT
    }
    }
    }

    If ($KEYFILE -eq "NO"){

      Write-Output "


       Logging you into KeePassX..."

    & $KeePassDataBaseLocation
    Start-Sleep -m $PauseTime
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
    [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
    [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
    $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
    [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
    [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
    Start-Sleep -m $PauseTime
    [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
    1..5 | % {
    [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
    [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
    [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
    [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
    Exit
    }Else{
    }

    If ($KEYFILE -eq "YES"){
      If (Test-Path $KEYFILELocation) {

        Write-Output "


       Logging you into KeePassX..."

      & $KeePassDataBaseLocation
      Start-Sleep -m $PauseTime
      [void] [System.Reflection.Assembly]::LoadWithPartialName("'Microsoft.VisualBasic")
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [void] [System.Reflection.Assembly]::LoadWithPartialName("'System.Windows.Forms")
      $Password = Import-Clixml -Path "$ENCRYPTIONFILE"
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait($Password.GetNetworkCredential().Password)
      1..4 | % {
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait("{TAB}")}
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      Start-Sleep -m $PauseTime
      [System.Windows.Forms.SendKeys]::SendWait("$KEYFILELocation")
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      Start-Sleep -m $PauseTime
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait("{TAB}")
      [Microsoft.VisualBasic.Interaction]::AppActivate("$DatabaseName - KeePassX")
      [System.Windows.Forms.SendKeys]::SendWait("{ENTER}")
      EXIT
      }Else{

        Write-Output "


      Please Insert Security USB..."
          Sleep 3
          Exit
        }
      }
    }
  }
}
#----------------------------------------------------------ENCRYPTION FILE DETECTION AND EXECUTION------------------------------------------

If (Test-Path $KeePassUsrDataLocation){
  $ENCRYPTIONFILE = $KeePassUsrDataLocation
  KeePassAutomation
  Exit
}
If (Test-Path $KeePassUsrDataLocation2){
  $ENCRYPTIONFILE = $KeePassUsrDataLocation2
  KeePassAutomation
  Exit
}
If (Test-Path $KeePassUsrDataLocation3){
  $ENCRYPTIONFILE = $KeePassUsrDataLocation3
  KeePassAutomation
  Exit
}
If (Test-Path $KeePassUsrDataLocation4){
  $ENCRYPTIONFILE = $KeePassUsrDataLocation4
  KeePassAutomation
  Exit
}
If (Test-Path $KeePassUsrDataLocation5){
  $ENCRYPTIONFILE = $KeePassUsrDataLocation5
  KeePassAutomation
  Exit
}
#----------------------------------------------------------------------------------------------------------------------------------------------
