$Host.UI.RawUI.WindowTitle = 'KeePass Backup 2.0'

$console = $host.UI.RawUI

$ErrorActionPreference= 'silentlycontinue'

Import-Module .\Modules\SetConsoleFont.psm1

$Config = "KeePassLauncher.Config"

Function KeePassBackup{

  $Config = "KeePassBackup.Config"

  If (Test-Path $Config){
  }Else{
    write-output{[KeePassBackupSettings]
DatabaseLocation=
BackupFolder=Z:\KeePass\Backup

[ConsoleSettings]
Background=Black
Foreground=White
FontSize=6

[Startup]
Startup=0} | add-content "KeePassBackup.Config"}

$file = get-content "KeePassBackup.Config"
$file | foreach {
  $items = $_.split("=")
  if ($items[0] -eq "DatabaseLocation"){$DatabaseLocation = $items[1]}
  if ($items[0] -eq "BackupFolder"){$BackupFolder = $items[1]}
  if ($items[0] -eq "Startup"){$Startup = $items[1]}
  if ($items[0] -eq "Background"){$Background = $items[1]}
  if ($items[0] -eq "Foreground"){$Foreground = $items[1]}
  if ($items[0] -eq "FontSize"){$FontSize = $items[1]}

  $console.ForegroundColor = $Foreground
  $console.BackgroundColor = $Background
  Set-ConsoleFont $FontSize
  cls

If ($Startup -eq "0"){

  $size = $console.WindowSize
  $size.Width = 87
  $size.Height = 8
  $console.WindowSize = $size

  (get-content "KeePassBackup.Config") | foreach-object {$_ -replace "0", "1"} | set-content "KeePassBackup.Config"
  Write-Output "


   FIRST TIME USE: Please ensure KeepassBackup.Config file is configured correctly..."
  sleep 4
  Exit}

  $size = $console.WindowSize
  $size.Width = 35
  $size.Height = 7
  $console.WindowSize = $size

  If (Test-Path $Config){
  }Else{
    Write-Output "


        Config file not found..."
    Sleep 3
    Exit
  }
  }

  $DatabaseName = Split-Path $DatabaseLocation -leaf

        $date=(get-date -Format d) -replace("/")
        $time=(get-date -Format t) -replace(":")
        $source = $DatabaseLocation
        $newfilename = "$date"+"_"+"$time"+".kdbx"

        If ($DatabaseLocation -eq ""){
          Write-Output "


        Database Not Found!"
          Sleep 3
          Exit
        }

        If (Test-Path $DatabaseLocation){
        }Else{
          Write-Output "


        Database Not Found!"
          Sleep 3
          Exit
        }

        If ($BackupFolder -eq ""){

          Write-Output "


        Backup Folder Not Found!"
          Sleep 3
          Exit
        }

        If (Test-Path $BackupFolder){
          }else{
            New-Item -ItemType Directory -Force -Path $BackupFolder | Out-Null
        }
        If (Test-Path "$BackupFolder\$newfilename"){

          Write-output "


      Backup already exists..."
          sleep 3
          Exit
        }else{
          Copy-Item $DatabaseLocation $BackupFolder
          If (Test-Path "$BackupFolder\$DatabaseName"){
            Rename-Item "$BackupFolder\$DatabaseName" -NewName $newfilename
            Write-Output "


          Backup Complete!"
            sleep 1
        Exit
      }
    }
  }

Function KeePassBackupDefault {

  $size = $console.WindowSize
  $size.Width = 35
  $size.Height = 7
  $console.WindowSize = $size

$Config = "KeePassLauncher.Config"

$file = get-content "KeePassLauncher.Config"
$file | foreach {
  $items = $_.split("=")
  if ($items[0] -eq "DatabaseLocation"){$DatabaseLocation = $items[1]}
  if ($items[0] -eq "Backup"){$Backup = $items[1]}
  if ($items[0] -eq "BackupFolder"){$BackupFolder = $items[1]}
  if ($items[0] -eq "Background"){$Background = $items[1]}
  if ($items[0] -eq "Foreground"){$Foreground = $items[1]}
  if ($items[0] -eq "FontSize"){$FontSize = $items[1]}
}

$console.ForegroundColor = $Foreground
$console.BackgroundColor = $Background
Set-ConsoleFont $FontSize
cls

If (Test-Path $Config){
}Else{
  Write-Output "


      Config file not found..."
  Sleep 3
  Exit
}


$DatabaseName = Split-Path $DatabaseLocation -leaf

      $date=(get-date -Format d) -replace("/")
      $time=(get-date -Format t) -replace(":")
      $source = $DatabaseLocation
      $newfilename = "$date"+"_"+"$time"+".kdbx"

      If ($DatabaseLocation -eq ""){
        Write-Output "


        Database Not Found!"
        Sleep 3
        Exit
      }

      If (Test-Path $DatabaseLocation){
      }Else{
        Write-Output "


        Database Not Found!"
        Sleep 3
        Exit
      }

      If ($BackupFolder -eq ""){

        Write-Output "


      Backup Folder Not Found!"
        Sleep 3
        Exit
      }

      If (Test-Path $BackupFolder){
        }else{
          New-Item -ItemType Directory -Force -Path $BackupFolder | Out-Null
      }
      If (Test-Path "$BackupFolder\$newfilename"){
        Write-output "


      Backup already exists..."
        sleep 3
        Exit
      }else{
        Copy-Item $DatabaseLocation $BackupFolder
        If (Test-Path "$BackupFolder\$DatabaseName"){
          Rename-Item "$BackupFolder\$DatabaseName" -NewName $newfilename
          Write-Output "


          Backup Complete!"
          sleep 1
      Exit
    }
  }
}

If (Test-Path $Config){
  KeePassBackupDefault
}Else{
  KeePassBackup
}
