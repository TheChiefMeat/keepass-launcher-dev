$Host.UI.RawUI.WindowTitle = 'AES KEY Generator 2.0'

$console = $host.UI.RawUI

$console.ForegroundColor = "white"
$console.BackgroundColor = "black"

$size = $console.WindowSize
$size.Width = 36
$size.Height = 8
$console.WindowSize = $size

If (Test-Path "AES.key"){
  Write-Output "


    AES.key file already exists!"
  sleep 4
  EXIT
}else{

  $KeyFile = "AES.key"
  $Key = New-Object Byte[] 32   # You can use 16, 24, or 32 for AES
  [Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($Key)
  $Key | out-file $KeyFile

  Write-Output "


      AES KEY file Generated!"
  sleep 4
  EXIT
}
