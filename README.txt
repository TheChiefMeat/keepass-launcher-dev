(C) TheChiefMeat 2016. All rights reserved.

KeePass Launcher is a tool designed to automatically launch your KeePass database
without any user intervention, find information about added features in the below
patch notes.

Download link:

https://bitbucket.org/TheChiefMeat/keepass-launcher/downloads/keepass-launcher-latest.zip

Requirements:

OS: Windows OS with Powershell version 5.0 (untested on earlier versions).
An administrative account with privileges for Powershell (see ElevatePermissions.reg).
KeePass database (.kdbx) file association already setup to use KeePass or KeeWeb.

Installation:

Extract the .zip file to a location of your choice.
Run the KeePass Launcher.exe
Follow the displayed instructions regarding configuration and setup.

Features:

Support for password only, password & keyfile and keyfile only modes.
Credential encryption.
Support for KeePass installation, portable KeePass, Desktop KeeWeb and KeePassX.
Customisable locations for all relevant files (see generated KeePassLauncher.Config at startup).
Automatic backup of database.

For further support email me at KeePassLauncherSL@gmail.com.

 Known Restrictions/Limitations:

 Using a key-file on a USB stick with other files/folders present on that stick may result
 in the wrong file being selected. Try to use a USB stick with the key-file being the
 only file on that stick. (This is no longer the case with KeePass Launcher 1.3+).

 Special characters cannot be used as part of your "MASTER PASSWORD" due to limitations
 with the "Send-Keys" command, this is a limitation with Powershell. Normal letters,
 capital letters, numbers and normal characters should be fine. For reference for special
 characters, check KeePass's Password Generator under "Tools".

 Powershell by default does not play very well with folders that contain spaces in the
 path of the folder. To remedy this, use the included registry file found in the "Extras"
 folder.

 PowerShell scripts generally require some form of elevated permission to run the script.
 In the "Extras" folder you can find a "ElevatePermissions.reg" file which will give you the
 appropriate permissions.

 If you are using KeePassX, ensure to untick the "Remember last databases", "Remember last key files"
 and "Open previous databases on startup" options as these options may cause issues.
